<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>

<body>
    <h1>Buat Account Baru!!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label><br />
        <input type="text" name="firstName" /><br /><br />
        <label>Last Name:</label><br />
        <input type="text" name="lastName" /><br /><br />
        <label for="gender">Gender:</label><br />
        <input type="radio" name="gender" id="m" /><label for="m">Male</label><br />
        <input type="radio" name="gender" id="f" /><label for="f">Female</label><br />
        <input disabled type="radio" id="o" /><label for="o">Other</label><br /><br />
        <label for="negara">Nationality:</label><br />
        <select name="negara" id="">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">Singapura</option>
        </select><br /><br />
        <label for="bahasa">Language Spoken:</label><br />
        <input type="checkbox" id="1" /><label for="1">Indonesia</label><br />
        <input type="checkbox" id="2" /><label for="2">English</label><br />
        <input type="checkbox" id="3" /><label for="3">Other</label><br /><br />
        <label for="bio">Bio:</label><br />
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br /><br />
        <input type="submit" value="Sign Up" />
    </form>
</body>

</html>
