<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $data = DB::table('cast')->get();
        return view('cast.index', ['data' => $data]);
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $input)
    {
        $input->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required'
        ]);

        DB::table('cast')->insert([
            'nama' => $input['nama'],
            'umur' => $input['umur'],
            'bio' => $input['bio']
        ]);

        return redirect('/cast');
    }

    public function show($id)
    {
        $data = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail', ['data' => $data]);
    }

    public function edit($id)
    {
        $data = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', ['data' => $data]);
    }

    public function update($id, Request $input)
    {
        $input->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required'
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $input['nama'],
                'umur' => $input['umur'],
                'bio' => $input['bio']
            ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
