@extends('layout.master')
@section('title')
    Form Edit Cast
@endsection

@section('content')
    <form action="/cast/{{ $data->id }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{ $data->nama }}">
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" name="umur" id="umur" value="{{ $data->umur }}">
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" name="bio" id="bio">{{ $data->bio }}</textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
