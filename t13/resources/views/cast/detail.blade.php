@extends('layout.master')
@section('title')
    Detail Cast
@endsection

@section('content')
    <h1>Nama Cast : {{ $data->nama }}</h1>
    <h3>Umur Cast : {{ $data->umur }}</h3>
    <p>Bio Cast : {{ $data->bio }}</p>
@endsection
