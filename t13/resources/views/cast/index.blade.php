@extends('layout.master')
@section('title')
    Index Cast
@endsection

@section('content')
    <a class="btn btn-primary mb-3" href="/cast/create" role="button">Add Cast</a>
    <table class="table table-striped table-bordered text-center">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data as $key =>$item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>{{ $item->bio }}</td>
                    <td>
                        <form action="/cast/{{ $item->id }}" method="post">
                            <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{ $item->id }}/edit" class="btn btn-success btn-sm">Update</a>
                            @method('delete')
                            @csrf
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                </tr>

            @empty
            @endforelse

        </tbody>
    </table>
@endsection
