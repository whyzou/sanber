<?php

require 'animal.php';
require 'ape.php';
require 'frog.php';


$sheep = new Animal("shaun");

echo "Nama = " . $sheep->name . "<br>";
echo "Jumlah Kaki = " . $sheep->legs . "<br>";
echo "Berdarah Dingin? = " . $sheep->cold_blooded . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Nama = " . $sungokong->name . "<br>";
echo "Jumlah Kaki = " . $sungokong->legs . "<br>";
echo "Berdarah Dingin? = " . $sungokong->cold_blooded . "<br>";
echo "Yell = " . $sungokong->yell();
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Nama = " . $kodok->name . "<br>";
echo "Jumlah Kaki = " . $kodok->legs . "<br>";
echo "Berdarah Dingin? = " . $kodok->cold_blooded . "<br>";
echo "Jump = " . $kodok->jump();
echo "<br>";
